from .keys import (
    PEXELS_API_KEY as pKey,
    OPEN_WEATHER_API_KEY as wKey
    )
import requests
import json


def get_location_image(city, state):
    pexel_url = f"https://api.pexels.com/v1/search?query={city}, {state}"
    header = {"Authorization": pKey}
    response = requests.get(pexel_url, headers=header)
    parsed_response = response.json()
    loc_image = {
        "image_url": parsed_response["photos"][0]["src"]["original"]
    }
    return loc_image


def get_weather_data(city, state):
    geocode_url = (
        f"http://api.openweathermap.org/geo/1.0/direct?"
        f"q={city},{state},&appid={wKey}"
        )
    lat_lon_response = requests.get(geocode_url)
    parsed_lat_lon_response = lat_lon_response.json()
    try:
        lat_lon = {
            "lat": parsed_lat_lon_response[0]["lat"],
            "lon": parsed_lat_lon_response[0]["lon"]
        }
    except(KeyError, IndexError):
        return None

    weather_url = (
        f"https://api.openweathermap.org/data/2.5/weather?lat={lat_lon['lat']}"
        f"&lon={lat_lon['lon']}&units=imperial&appid={wKey}"
        )
    weather_response = requests.get(weather_url)
    parsed_weather_response = json.loads(weather_response.content)

    try:
        current_weather = {
            "temp": parsed_weather_response["main"]["temp"],
            "description": parsed_weather_response["weather"][0]["description"]
        }
        return current_weather
    except(KeyError, IndexError):
        return None
